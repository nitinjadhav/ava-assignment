# Coding Assignment

### Features
* Modular code, ECMA2015 classes used
* Cache used to avoid repeated calculations
* Optimized code, cache is built as-you-go
* Pure JS with Parcel bundler, no third-party library used
* CSS Grid, CSS variables used for color selection

### Instructions:

1. run `yarn` or `npm install` in root dir of project.
2. run `yarn start` or `npm run start`

### Testing
run `yarn test`.
Note that only one test is included, due to time constraint.

### Note
I implemented only foreground colour selection due to time constraint, but background  grid colour can be done similarly.

### Todo
I would have done these things if more time was available:
- CSS using BEM methodology
- More tests
- Background colour selection
- CSS Modules