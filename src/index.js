import "./styles.css";
import { Matrix } from "./Matrix/Matrix";

const DEFAULT_GRID_SIZE = 6;

const container = document.getElementById("app");

//create re-usable matrix instance
const matrix = new Matrix(container);

matrix.generateMatrix(DEFAULT_GRID_SIZE, DEFAULT_GRID_SIZE);

//update UI to reflect grid size
updateUIGridSize(DEFAULT_GRID_SIZE);

//add event handlers
window.addEventListener("DOMContentLoaded", (event) => {
  document
    .getElementById("matrix-size-slider")
    .addEventListener("change", function (event) {
      const selectedSize = Number(event.target.value);
      matrix.generateMatrix(selectedSize, selectedSize);
      updateUIGridSize(selectedSize);
    });
  document
    .getElementById("square-color")
    .addEventListener("change", function (event) {
      // debugger;
      console.log(event.target.value);
      matrix.setGridColor(event.target.value);
    });
});

function updateUIGridSize(newSize){
  document.querySelector('.grid-size').innerHTML = `${newSize}X${newSize}`;
}