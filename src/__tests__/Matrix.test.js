import { Matrix } from "../Matrix/Matrix";
import { JSDOM } from 'jsdom';

test("Matrix class: generateArray() should create new nxn array with random 0 or 1s", () => {
    const array = Matrix.generateArray(2, 2);
    expect(array).toHaveLength(2);
    expect(array[0][0] === 0 || array[0][0] === 1).toBeTruthy();
})