export function generateRandom0or1() {
  const random01 = Number(Math.random() >= 0.5);
  return random01;
}

export function removeAllChildren(domElement) {
  if (!domElement) {
    return;
  }
  while (domElement.firstChild) {
    domElement.firstChild.remove();
  }
}
