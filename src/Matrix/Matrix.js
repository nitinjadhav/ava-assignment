import { removeAllChildren, generateRandom0or1 } from "../helpers";
import "./Matrix.css";

/* Encapsulates matrix with clickable squares functionality */
export class Matrix {

  // # represents esnext private variable

  #container;
  #matrix;
  #rowsCount;
  #colsCount;
  #previousSquareElement

  /*
   * Cache to store connected elements groups ("islands")
   * format: [['0-1','0-2'],['3-2','3-3','3-4','4-3']]
   */
  #islands = [];

  constructor(parentElement) {
    parentElement.classList.add('grid-container');
    this.#container = parentElement;
  }

  //Creates and renders new grid with provided size
  generateMatrix(rows, cols) {

    //generate random array with given size
    this.#matrix = Matrix.generateArray(rows, cols);
    this.#rowsCount = rows;
    this.#colsCount = cols;

    //init cache
    this.#islands = [];

    //manipulate dom
    this.renderGrid();
    this.addEventHandlers();
    this.setGridSizeVariables(rows, cols);
  }

  // renders grid to dom
  renderGrid() {

    //empty container element
    removeAllChildren(this.#container);

    for (let row = 0;row < this.#rowsCount;row++) {
      for (let col = 0;col < this.#colsCount;col++) {
        const content = this.#matrix[row][col];
        const square = Matrix.createSquare(content, row, col);
        this.#container.append(square);
      }
    }
  }

  // Event Delegation: event handlers are added ONLY on the container, not on every square
  addEventHandlers() {
    this.#container.addEventListener("click", ({ target: squareElement }) => {

      //don't handle event if token attribute is missing
      const { token } = squareElement.dataset;
      if (!token) {
        return;
      }

      this.handleSquareClick(token, squareElement);
    });

    this.#container.addEventListener("mouseover", ({ target: squareElement }) => {

      //don't handle event if token attribute is missing
      const { token } = squareElement.dataset;
      if (!token) {
        return;
      }
      this.highlightNodesWithTokens(this.getIsland(token));
    });

    this.#container.addEventListener("mouseout", ({ target: squareElement }) => {

      //don't handle event if token attribute is missing
      const { token } = squareElement.dataset;
      if (!squareElement.dataset.token) {
        return;
      }

      this.removeNodeHighlightWithTokens(this.getIsland(token));
    });
  }

  handleSquareClick(token, squareElement) {
    const { connectedCount } = squareElement.dataset;
    if (connectedCount) {
      //already has count added
      return;
    }

    if (this.#previousSquareElement) {
      delete this.#previousSquareElement.dataset.connectedCount;
    }

    //its guaranteed that island is already present, build by mouseover vent
    squareElement.dataset["connectedCount"] = this.getIsland(token).length;
    this.#previousSquareElement = squareElement;
  }

  //Get or create and return island connected to current node token
  getIsland(token) {
    const foundIsland = this.#islands.find(group => group.includes(token));

    if (foundIsland) {
      return foundIsland;
    }

    //console.log('creating connected nodes island....');

    //crete connected island nad cache it
    const [row, column] = token.split('-').map(Number);

    //recursive call to this method!
    const island = this.traverseNode(row, column);

    this.#islands.push(island);
    return island;
  }

  highlightNodesWithTokens(tokens) {
    tokens.forEach(token => {
      const node = this.#container.querySelector(`[data-token="${token}"]`);
      //add highlight attribute to node
      node.dataset.highlight = "";
    })
  }

  removeNodeHighlightWithTokens(tokens) {
    tokens.forEach(token => {
      const node = this.#container.querySelector(`[data-token="${token}"]`);
      delete node.dataset.highlight;
    })
  }

  //uses CSS variables to set CSS GRID row/column templates
  setGridSizeVariables(rows, cols) {
    this.#container.style.setProperty("--grid-cols", cols);
    this.#container.style.setProperty("--grid-rows", rows);
  }

  setGridColor(colour) {
    this.#container.style.setProperty("--grid-highlight-color", colour);
  }

  //Traverse matrix node to find connected square islands
  traverseNode(row, col) {
    //set 0 here, effectively nullifying node
    this.#matrix[row][col] = 0;

    const island = [Matrix.getToken(row, col)];
    const neighbors = [[row - 1, col], [row + 1, col], [row, col - 1], [row, col + 1]];

    for (let [currentRow, currentCol] of neighbors) {
      if (currentRow >= 0 && currentRow < this.#rowsCount &&
        currentCol >= 0 && currentCol < this.#colsCount &&
        this.#matrix[currentRow][currentCol]) {
        //valid row/column and element is 1
        const connectedNodes = this.traverseNode(currentRow, currentCol);
        island.push(...connectedNodes);
      }
    }

    return island;
  }

  static generateArray(m, n) {
    let newMatrix = new Array(m).fill(0);
    newMatrix = newMatrix.map(() => new Array(n).fill(0).map(() => generateRandom0or1()));
    return newMatrix;
  }

  static getToken(value1, value2) {
    return `${value1}-${value2}`;
  }

  // creates one square DOM node in this grid
  static createSquare(content, row, column) {
    const div = document.createElement("div");
    div.classList.add("square");

    if (content) {
      div.dataset.token = Matrix.getToken(row, column);
    }
    return div;
  }
}
